using NUnit.Framework;
using Moq;
using System;

namespace HeatingControlSystem
{
    public interface IHeatingControlSystem
    {
        // Interface for a new heating control system
        string MaintainTemperature(TimeSpan currentTime, int currentTemperature);
    }

    public class HeatingControlSystem : IHeatingControlSystem
    {
        private TemperatureSensor temperatureSensor;
        private HeatingSystem heatingSystem;

        /// <summary>
        /// A new heating control system is built with a temperature sensor, and heating system
        /// </summary>
        /// <param name="tempSensor"></param>
        /// <param name="heatSystem"></param>
        public HeatingControlSystem(TemperatureSensor tempSensor, HeatingSystem heatSystem )
        {
            temperatureSensor = tempSensor;
            heatingSystem = heatSystem;
        }

        public virtual string MaintainTemperature(TimeSpan currentTime, int currentTemperature)
        {
            temperatureSensor.CheckTemperature();
            var currentStatus = heatingSystem.CheckHeatingStatus();

            return currentStatus;
        }
    }

    public abstract class TemperatureSensor
    {
        // Abstract class for a new temperature sensor

        public virtual decimal CheckTemperature()
        {
            throw new NotImplementedException();
        }
    }

    public abstract class HeatingSystem
    {
        // Abstract class for a new heating system

        public virtual string CheckHeatingStatus()
        {
            throw new NotImplementedException();
        }
    }

    [TestFixture]
    public class Tests
    {
        Mock<HeatingSystem> mockHeatingSystem;
        Mock<TemperatureSensor> mockTemperatureSensor;

        HeatingControlSystem heatingControlSystem;

        [SetUp]
        public void Setup()
        {
            // Setup the mock heathing control system, heating system, and temperature sensor
            mockHeatingSystem = new Mock<HeatingSystem>(MockBehavior.Strict);
            mockTemperatureSensor = new Mock<TemperatureSensor>(MockBehavior.Strict);
        }

        [Test]
        [TestCase("10:00", 30, "cool")]
        [TestCase("10:00", 10, "heat")]
        [TestCase("10:00", 20, "off")]
        [TestCase("10:00", null, "off")]
        public void TestHeatingControl(TimeSpan currentTimePeriod, int currentTemperature, string expectedResult)
        {
            // Mock the heating system and temperature sensor abstract classes
            mockHeatingSystem.Setup(x => x.CheckHeatingStatus()).Returns(expectedResult);
            mockTemperatureSensor.Setup(x => x.CheckTemperature()).Returns(currentTemperature);

            // Setup a new hcs object
            heatingControlSystem = new HeatingControlSystem(mockTemperatureSensor.Object, mockHeatingSystem.Object);

            // Get the response from maintain temp
            var actualResult = heatingControlSystem.MaintainTemperature(currentTimePeriod, currentTemperature);

            mockHeatingSystem.Verify(v => v.CheckHeatingStatus(), Times.Once);
            mockTemperatureSensor.Verify(v => v.CheckTemperature(), Times.Once);

            Assert.AreEqual(expectedResult, actualResult, "Data does not match");
        }
    }
}